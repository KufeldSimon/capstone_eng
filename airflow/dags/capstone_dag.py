from datetime import datetime, timedelta
import os
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from operators import (CopyToS3Operator, StageToRedshiftOperator, DataQualityOperator, LoadTableOperator)
from helpers import SqlStatements, apply_transformations


# AWS_KEY = os.environ.get('AWS_KEY')
# AWS_SECRET = os.environ.get('AWS_SECRET')

debug = False

default_args = {
    'owner': 'skufeld',
    'start_date': datetime(2019, 1, 12),
    'depends_on_past': False,
    'retries': 3,
    'retry_delay': timedelta(minutes=5),
    'email_on_retry': False,
    'catchup': False,
}

dag = DAG('udac_example_dag',
          default_args=default_args,
          description='ELT process with Airflow from S3 to Redshift',#
          schedule_interval='@yearly',
          max_active_runs=1,
          )

start_operator = DummyOperator(task_id='Begin_execution', dag=dag)

fransform_raw_data = PythonOperator(
    task_id='Transform_raw',
    provide_context=True,
    python_callable=apply_transformations,
    op_kwargs={'source_path': '/Users/skufeld/capstone/raw_data',
               'target_path': '/Users/skufeld/capstone/transformed_data'},
    dag=dag,
)

copy_to_s3 = CopyToS3Operator(
        task_id='Copy_to_S3_bucket',
        dag=dag,
        basepath='/Users/skufeld/capstone/transformed_data',
        s3_bucket='capstone',
        aws_connection_id='aws_credentials',
        aws_default_region='us-west-2',
)

stage_immigration_to_redshift = StageToRedshiftOperator(
    task_id='Stage_events',
    dag=dag,
    provide_context=True,
    ddl_query=SqlStatements.staging_immigration_ddl,
    db='public',
    table='staging_immigration',
    aws_credentials_id='aws_credentials',
    redshift_conn_id='redshift',
    s3_bucket='capstone',
    s3_key='immigration',
)

stage_demographic_to_redshift = StageToRedshiftOperator(
    task_id='Stage_events',
    dag=dag,
    provide_context=True,
    ddl_query=SqlStatements.staging_demographic_ddl,
    db='public',
    table='staging_demographic',
    aws_credentials_id='aws_credentials',
    redshift_conn_id='redshift',
    s3_bucket='capstone',
    s3_key='demographic',
)

stage_airport_info_to_redshift = StageToRedshiftOperator(
    task_id='Stage_events',
    dag=dag,
    provide_context=True,
    ddl_query=SqlStatements.staging_airport_info_ddl,
    db='public',
    table='staging_airport_info',
    aws_credentials_id='aws_credentials',
    redshift_conn_id='redshift',
    s3_bucket='capstone',
    s3_key='airport',
)

stage_airport_codes_to_redshift = StageToRedshiftOperator(
    task_id='Stage_events',
    dag=dag,
    provide_context=True,
    ddl_query=SqlStatements.staging_airport_codes_ddl,
    db='public',
    table='staging_airport_codes',
    aws_credentials_id='aws_credentials',
    redshift_conn_id='redshift',
    s3_bucket='capstone',
    s3_key='airport_codes',
)

stage_country_codes_to_redshift = StageToRedshiftOperator(
    task_id='Stage_events',
    dag=dag,
    provide_context=True,
    ddl_query=SqlStatements.staging_country_codes_ddl,
    db='public',
    table='staging_country_codes',
    aws_credentials_id='aws_credentials',
    redshift_conn_id='redshift',
    s3_bucket='capstone',
    s3_key='country_codes',
)
stating_tables = [
    {'check_sql': SqlStatements.dq_count_table.__add__("public.staging_immigration"), 'unexp_result': 0},
    {'check_sql': SqlStatements.dq_count_table.__add__("public.staging_demographic"), 'unexp_result': 0},
    {'check_sql': SqlStatements.dq_count_table.__add__("public.staging_airport_info"), 'unexp_result': 0},
    {'check_sql': SqlStatements.dq_count_table.__add__("public.staging_airport_codes"), 'unexp_result': 0},
    {'check_sql': SqlStatements.dq_count_table.__add__("public.staging_country_codes"), 'unexp_result': 0},
]

run_checks_staging = DataQualityOperator(task_id='Run_staging_table_checks',
                                         dag=dag,
                                         redshift_conn_id='redshift',
                                         dq_checks=stating_tables)

load_immigration_fact_table = LoadTableOperator(
    task_id='Load_immigration_fact_table',
    dag=dag,
    redshift_conn_id='redshift',
    db='public',
    table='fact_immigration',
    truncate=False,
    dml_query=SqlStatements.fact_immigration_dml,
)

load_timetable_dimension_table = LoadTableOperator(
    task_id='Load_timetable_dim_table',
    dag=dag,
    redshift_conn_id='redshift',
    db='public',
    table='dim_timetable',
    truncate=False,
    dml_query=SqlStatements.dim_timetable_dml,
)

load_demographic_dimension_table = LoadTableOperator(
    task_id='Load_demographic_dim_table',
    dag=dag,
    redshift_conn_id='redshift',
    db='public',
    table='dim_demographic',
    truncate=False,
    dml_query=SqlStatements.dim_demographic_dml,
)

load_airport_dimension_table = LoadTableOperator(
    task_id='Load_airport_dim_table',
    dag=dag,
    redshift_conn_id='redshift',
    db='public',
    table='dim_airport',
    truncate=False,
    dml_query=SqlStatements.dim_airport_dml,
)

load_city_dimension_table = LoadTableOperator(
    task_id='Load_city_dim_table',
    dag=dag,
    redshift_conn_id='redshift',
    db='public',
    table='dim_city',
    truncate=False,
    dml_query=SqlStatements.dim_city_dml,
)

final_tables = [
    {'check_sql': SqlStatements.dq_count_table.__add__("public.fact_immigration"), 'unexp_result': 0},
    {'check_sql': SqlStatements.dq_count_table.__add__("public.dim_timetable"), 'unexp_result': 0},
    {'check_sql': SqlStatements.dq_count_table.__add__("public.dim_demographic"), 'unexp_result': 0},
    {'check_sql': SqlStatements.dq_count_table.__add__("public.dim_airport"), 'unexp_result': 0},
    {'check_sql': SqlStatements.dq_count_table.__add__("public.dim_city"), 'unexp_result': 0},
]

run_checks_final_tables = DataQualityOperator(task_id='Run_final_table_checks',
                                              dag=dag,
                                              redshift_conn_id='redshift',
                                              dq_checks=final_tables)

end_operator = DummyOperator(task_id='Stop_execution', dag=dag)

staging_dml_list = [stage_immigration_to_redshift,
                    stage_demographic_to_redshift,
                    stage_airport_info_to_redshift,
                    stage_airport_codes_to_redshift,
                    stage_country_codes_to_redshift]

final_dml_list = [load_immigration_fact_table,
                  load_timetable_dimension_table,
                  load_demographic_dimension_table,
                  load_airport_dimension_table,
                  load_city_dimension_table]

start_operator >> fransform_raw_data

fransform_raw_data >> copy_to_s3

copy_to_s3 >> staging_dml_list

staging_dml_list >> run_checks_staging

run_checks_staging >> final_dml_list

final_dml_list >> run_checks_final_tables

run_checks_final_tables >> end_operator
