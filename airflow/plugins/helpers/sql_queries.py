class SqlStatements:
# DQ
    dq_count_table = ("""
        SELECT
            count(1)
        FROM
    """)

# Staging DDL
    staging_immigration_ddl = """
        CREATE TABLE IF NOT EXISTS public.staging_immigration
        (
         id_immigration double precision NOT NULL,
         airport_code   bigint NOT NULL,
         arrival_date   date NOT NULL,
         departure_date date,
         gender         varchar(1),
         birth_year     date,
         country_code   bigint,
         visa_category  integer,
         visa_type      varchar(5),
         state_short    varchar(5),
         CONSTRAINT id_immigration PRIMARY KEY ( id_immigration )
        );
    """

    staging_country_codes_ddl = """
        CREATE TABLE IF NOT EXISTS public.staging_country_codes
        (
         country_code integer NOT NULL,
         country_long varchar(50) NOT NULL,
         CONSTRAINT country_codes_pkey PRIMARY KEY ( country_code )
        );
    """

    staging_airport_codes_ddl = """
        CREATE TABLE IF NOT EXISTS public.staging_airport_codes
        (
         airport_short varchar(50) NOT NULL,
         airport_long  varchar(50) NOT NULL,
         CONSTRAINT airport_codes_pkey PRIMARY KEY ( airport_short )
        );
    """

    staging_demographic_ddl = """
        CREATE TABLE IF NOT EXISTS public.staging_demographic
        (
         city             varchar(3) NOT NULL,
         total_population bigint NOT NULL,
         foreign_born     bigint NOT NULL,
         state            varchar(50),
         state_code       varchar(50),
         median_age       double precision,
         asian            bigint,
         black            bigint,
         hispanic         bigint,
         native           bigint,
         white            bigint,
         CONSTRAINT demographic_pkey PRIMARY KEY ( city )
        );
    """

    staging_airport_info_ddl = """
        CREATE TABLE IF NOT EXISTS public.staging_airport_info
        (
         iata_code       varchar(3) NOT NULL,
         airport_name    varchar(50) NOT NULL,
         airport_type    varchar(50),
         airport_country varchar(50),
         airport_region  varchar(50),
         municipality    varchar(50),
         latitude        double precision,
         longitude       double precision,
         CONSTRAINT airport_info_pkey PRIMARY KEY ( iata_code, airport_name )
        );
    """

# DDL Fact Tables
    fact_immigration_ddl = """
        CREATE TABLE IF NOT EXISTS public.fact_immigration
        (
         id_immigration double precision NOT NULL,
         airport_code   bigint NOT NULL,
         arrival_date   date NOT NULL,
         departure_date date,
         gender         varchar(1),
         birth_year     date,
         visa_category  integer,
         visa_type      varchar(5),
         country_code   varchar(50),
         country_long   varchar(50),
         CONSTRAINT id_immigration PRIMARY KEY ( id_immigration )
        );
    """
# DDL Dim Tables

    dim_timetable_ddl = """
        CREATE TABLE IF NOT EXISTS dim_timetable
        (
         date         date NOT NULL,
         year         bigint NOT NULL,
         month        bigint NOT NULL,
         day          bigint NOT NULL,
         day_of_week  varchar(50) NOT NULL,
         is_weekday   boolean NOT NULL,
         CONSTRAINT timetable_pkey PRIMARY KEY ( date )
        );
    """


    dim_demographic_ddl = """
        CREATE TABLE IF NOT EXISTS dim_demographic
        (
         city             varchar(3) NOT NULL,
         total_population bigint NOT NULL,
         foreign_born     bigint NOT NULL,
         median_age       double precision,
         asian            bigint,
         black            bigint,
         hispanic         bigint,
         native           bigint,
         white            bigint,
         CONSTRAINT dim_demographic_pkey PRIMARY KEY ( city )
        );
    """


    dim_airport_ddl = """
        CREATE TABLE IF NOT EXISTS dim_airport
        (
         iata_code    varchar(3) NOT NULL,
         airport_name varchar(50) NOT NULL,
         city         varchar(50) NOT NULL,
         airport_type varchar(50) NOT NULL,
         latitude     double precision NOT NULL,
         longitude    double precision NOT NULL,
         airport_long_name varchar(50) NOT NULL,
         CONSTRAINT PK_Product PRIMARY KEY ( iata_code, airport_name )
        );
    """
    dim_city_ddl = """
        CREATE TABLE IF NOT EXISTS dim_city
        (
         city            varchar(3) NOT NULL,
         state           varchar(50),
         state_code      varchar(50),
         airport_country varchar(50),
         airport_region  varchar(50),
         CONSTRAINT PK_Product PRIMARY KEY ( city )
        );
    """

# DML Fact Table
    fact_immigration_dml = """
        WITH temp_table AS(
            immi.id_immigration,
            immi.airport_code,
            immi.arrival_date,
            immi.departure_date,
            immi.gender,
            immi.birth_year,
            immi.visa_category,
            immi.visa_type,
            immi.country_code,
            codes.country_long
        FROM public.staging_immigration immi 
            LEFT JOIN public.staging_country_codes codes
            ON immi.country_code == codes.country_code
        )
        INSERT OVERWRITE INTO fact_immigration (
            id_immigration,
            airport_code,
            arrival_date,
            departure_date,
            gender,
            birth_year,
            visa_category,
            visa_type,
            country_code,
            country_long)
        SELECT DISTINCT
            id_immigration,
            airport_code,
            arrival_date,
            departure_date,
            gender,
            birth_year,
            visa_category,
            visa_type,
            country_code,
            country_long
        FROM temp_table
        WHERE id_immigration IS NOT NULL
          AND airport_code IS NOT NULL
          AND arrival_date IS NOT NULL
    """

# DML Dim Table
    dim_timetable_dml = """
        WITH temp_table AS (
        SELECT arrival_date as input_date FROM public.staging_immigration
        UNION
        SELECT departure_date as input_date FROM public.staging_immigration
        )
        INSERT OVERWRITE INTO dim_timetable (
            date,
            year,
            month,
            day,
            day_of_week,
            is_weekday)
        SELECT DISTINCT
            input_date,
            extract(year from arrival_date),
            extract(month from arrival_date),
            extract(day from start_time),
            extract(dayofweek from start_time),
            CASE
            WHEN extract(dayofweek from start_time) == 0 THEN True
            WHEN extract(dayofweek from start_time) == 6 THEN True
            ELSE False
            END
        FROM temp_table
        WHERE input_date IS NOT NULL;
    """

    dim_demographic_dml = """
        INSERT OVERWRITE INTO dim_demographic (
            city,
            total_population,
            foreign_born,
            median_age,
            asian,
            black,
            hispanic,
            native,
            white)
        SELECT DISTINCT
            city,
            total_population,
            foreign_born,
            median_age,
            asian,
            black,
            hispanic,
            native,
            white
        FROM public.staging_demographic
        WHERE city IS NOT NULL
              AND total_population IS NOT NULL
              AND foreign_born IS NOT NULL;
    """

    dim_airport_dml = """
        WITH temp_table AS(
            extended.iata_code,
            extended.airport_name,
            extended.city,
            extended.airport_type,
            extended.latitude,
            extended.longitude,
            short.airport_long
            from public.staging_airport_info extended left join public.staging_airport_codes short
                ON extended.iata_code == short.airport_short
        )
        INSERT OVERWRITE INTO dim_airport (
            iata_code,
            airport_name,
            city,
            airport_type,
            latitude,
            longitude,
            airport_long_name)
        SELECT DISTINCT
            iata_code,
            airport_name,
            city,
            airport_type,
            latitude,
            longitude,
            airport_long
        FROM temp_table
        WHERE iata_code IS NOT NULL
        AND airport_name IS NOT NULL
        """

    dim_city_dml = """
        WITH temp_table AS(
        SELECT
            demo.city,
            demo.state,
            demo.state_code,
            air.airport_country,
            air.airport_region
        FROM staging_demographic demo
            LEFT JOIN staging_airport_info air
            ON demo.city == air.municipality
        
        )
        INSERT OVERWRITE INTO dim_city (
         city,
         state,
         state_code,
         airport_country,
         airport_region
        )
        SELECT DISTINCT
         city,
         state,
         state_code,
         airport_country,
         airport_region
        FROM temp_table
        WHERE city IS NOT NULL
    """

