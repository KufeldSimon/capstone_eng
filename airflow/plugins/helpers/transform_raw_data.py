from functools import reduce
from pyspark.sql.functions import udf, col, isNotNull
import datetime
from os import listdir
from os.path import isfile, isdir, join
from functools import reduce
from pyspark.sql.functions import udf, array
import pyspark.sql.types as T
from pyspark.sql import DataFrame
from pyspark.sql import SparkSession
import os
import logging

_logger = logging.getLogger(__name__)

immigration_column_list = ['cicid', 'i94cit', 'i94port', 'arrdate',
                           'depdate', 'i94addr', 'i94visa', 'visatype',
                           'gender', 'biryear', 'i94mon']

d_country_code = {'from_name': 'i94cit',
                  'to_name': 'country_code',
                  'from_type': T.StringType(),
                  'to_type': T.IntegerType()}
d_country_long = {'from_name': 'country',
                  'to_name': 'country_long',
                  'from_type': T.StringType(),
                  'to_type': T.StringType()}
country_codes_schema_def = [d_country_code, d_country_long]

d_port_short = {'from_name': 'i94port',
                'to_name': 'airport_short',
                'from_type': T.StringType(),
                'to_type': T.StringType()}
d_port_long = {'from_name': 'airport',
               'to_name': 'airport_long',
               'from_type': T.StringType(),
               'to_type': T.StringType()}
airport_codes_schema_def = [d_port_short, d_port_long]

d_type = {'from_name': 'type',
          'to_name': 'airport_type',
          'from_type': T.StringType(),
          'to_type': T.StringType()}
d_name = {'from_name': 'name',
          'to_name': 'airport_name',
          'from_type': T.StringType(),
          'to_type': T.StringType()}
d_country = {'from_name': 'iso_country',
             'to_name': 'airport_country',
             'from_type': T.StringType(),
             'to_type': T.StringType()}
d_region = {'from_name': 'iso_region',
            'to_name': 'airport_region',
            'from_type': T.StringType(),
            'to_type': T.StringType()}
d_municipality = {'from_name': 'iso_country',
                  'to_name': 'municipality',
                  'from_type': T.StringType(),
                  'to_type': T.StringType()}
d_iata = {'from_name': 'iata_code',
          'to_name': 'iata_code',
          'from_type': T.StringType(),
          'to_type': T.StringType()}
d_coordinates = {'from_name': 'coordinates',
                 'to_name': 'coordinates',
                 'from_type': T.StringType(),
                 'to_type': T.StringType()}
airport_schema_def = [d_type, d_name, d_country, d_region, d_municipality, d_iata, d_coordinates]

d_city = {'from_name': 'City',
          'to_name': 'city',
          'from_type': T.StringType(),
          'to_type': T.StringType()}

d_state = {'from_name': 'State',
           'to_name': 'state',
           'from_type': T.StringType(),
           'to_type': T.StringType()}

d_median_age = {'from_name': 'Median Age',
                'to_name': 'median_age',
                'from_type': T.StringType(),
                'to_type': T.IntegerType()}

d_total = {'from_name': 'Total Population',
           'to_name': 'total_population',
           'from_type': T.StringType(),
           'to_type': T.IntegerType()}

d_for_born = {'from_name': 'Foreign-born',
              'to_name': 'foreign_born',
              'from_type': T.StringType(),
              'to_type': T.IntegerType()}

d_state_code = {'from_name': 'State Code',
                'to_name': 'state_code',
                'from_type': T.StringType(),
                'to_type': T.StringType()}

d_race = {'from_name': 'Race',
          'to_name': 'census_race',
          'from_type': T.StringType(),
          'to_type': T.StringType()}

d_count = {'from_name': 'Count',
           'to_name': 'census_count',
           'from_type': T.StringType(),
           'to_type': T.IntegerType()}
demographic_schema_def = [d_city, d_state, d_median_age, d_total, d_for_born, d_state_code, d_race, d_count]


@udf(T.FloatType())
def construct_id(input_array):
    try:
        return float(str(int(input_array[0]))+str(int(input_array[1])))
    except ValueError:
        return None


@udf(T.DateType())
def transform_to_datetype(date_in_days):
    if date_in_days is None:
        return None
    return datetime.datetime(1960, 1, 1) + datetime.timedelta(days=int(date_in_days))


@udf(T.DateType())
def transform_birth_year(birthyear):
    if birthyear is None:
        return None
    return datetime.datetime(int(birthyear), 1, 1)


coordinates_schema = T.StructType([T.StructField("latitude", T.FloatType(), False),
                                   T.StructField("longitude", T.FloatType(), False)])


@udf(coordinates_schema)
def return_lat_long(string):
    lat_long_tuple = string.split(',')
    try:
        longitude = float(lat_long_tuple[0].strip())
        latitude = float(lat_long_tuple[1].strip())
    except ValueError:
        longitude, latitude = None, None
    return latitude, longitude


def get_filenames(path):
    return [f for f in listdir(path) if isfile(join(path, f))]


def get_foldernames(path):
    return [f for f in listdir(path) if isdir(join(path, f))]


spark = SparkSession.builder.config("spark.jars.packages",
                                    "saurfang:spark-sas7bdat:2.0.0-s_2.11").enableHiveSupport().getOrCreate()


def transform_immigration(source_path, target_path,
                          column_list):
    source_path = os.path.join(source_path, "immigration")
    target_path = os.path.join(target_path, "immigration")
    file_list = get_filenames(source_path)
    file_paths_list = [f'{source_path}/{file}' for file in file_list]
    df_list = []
    for file_path in file_paths_list:
        df_list.append((spark.read.format('com.github.saurfang.sas.spark').load(file_path)).select(column_list))
    df = reduce(DataFrame.unionAll, df_list)
    print(f"Rows in combined df: {df.count()}")
## transform
    df = df.withColumn('arrival_date', transform_to_datetype('arrdate')).drop('arrdate')
    df = df.withColumn('departure_date', transform_to_datetype('depdate')).drop('depdate')
    df = df.withColumn('birth_year', transform_birth_year('biryear')).drop('biryear')
    df = df.withColumn('id_immigration', construct_id(array('cicid', 'i94mon')).drop('cicid','i94mon')
#rename
    df = df.select('*', df.i94cit.cast(T.IntegerType()).alias('country_code')).drop('i94cit')
    df = df.select('*', df.i94port.cast(T.StringType()).alias('airport_code')).drop('i94port')
    df = df.select('*', df.i94visa.cast(T.StringType()).alias('visa_category')).drop('i94visa')
    df = df.select('*', df.visatype.cast(T.StringType()).alias('visa_type')).drop('visatype')
    df = df.select('*', df.i94addr.cast(T.StringType()).alias('state_code')).drop('i94addr')
    print(f"Rows after transforming: {df.count()}")
#filter nulls and duplicates
    df = df.na.drop(subset=['id_immigration', 'airport_code', 'arrival_date'])
    print(f"Rows after dropping NA: {df.count()}")
    df = df.drop_duplicates(subset=['id_immigration', 'airport_code', 'arrival_date'])
    # reduces the rows count from 40 mio to 7 mio
    print(f"Rows after dropping duplicates: {df.count()}")
    column_order = ["id_immigration", "airport_code", "arrival_date", "departure_date", "gender",
                    "birth_year", "country_code", "visa_category", "visa_type", "state_short"]
    df = df.select(*column_order)
    df.repartition(len(file_list)).write.option("compression", "none").parquet(target_path)


def transform_country_codes_csv(source_path, target_path,
                                schema_def):
    source_path = os.path.join(source_path, "I94_country_codes.csv")
    target_path = os.path.join(target_path, "country_codes")
    df = spark.read.options(delimiter=',', header=True).csv(source_path)
    select_columns = [column_def['from_name'] for column_def in schema_def]
    df = df.select(*select_columns)
    # rename
    data_schema = [T.StructField(column_def['to_name'], column_def['from_type'], True)
                   for column_def in schema_def]
    rename_schema = T.StructType(fields=data_schema)
    df = spark.createDataFrame(df.rdd, schema=rename_schema)
    # cast
    df = df.select([col(column_def['to_name']).cast(column_def['to_type']) for column_def in schema_def])
    df = df.dropDuplicates()
    df = df.na.drop(subset=['country_code', 'country_long'])
    column_order = ['country_code', 'country_long']
    df = df.select(*column_order)
    df.write.option("compression", "none").parquet(target_path)


def transform_airport_codes_csv(source_path, target_path,
                                schema_def):
    source_path = os.path.join(source_path, 'I94_port_codes.csv')
    target_path = os.path.join(target_path, "airport_codes")
    df = spark.read.options(delimiter=',', header=True).csv(source_path)
    select_columns = [column_def['from_name'] for column_def in schema_def]
    df = df.select(*select_columns)
    # rename
    data_schema = [T.StructField(column_def['to_name'], column_def['from_type'], True)
                   for column_def in schema_def]
    rename_schema = T.StructType(fields=data_schema)
    df = spark.createDataFrame(df.rdd, schema=rename_schema)
    # cast
    df = df.select([col(column_def['to_name']).cast(column_def['to_type']) for column_def in schema_def])
    df = df.dropDuplicates()
    df = df.na.drop(subset=['airport_short', 'airport_long'])
    column_order = ['airport_short', 'airport_long']
    df = df.select(*column_order)
    df.write.option("compression", "none").parquet(target_path)


def transform_airport_csv(source_path, target_path,
                          schema_def):
    source_path = os.path.join(source_path, "airport-codes_csv.csv")
    target_path = os.path.join(target_path, "airport")
    df = spark.read.options(delimiter=',', header=True, encoding="ISO-8859-1").csv(source_path)
    # encoding= "utf-8",
    # charset", "iso-8859-1")
    # select subset
    select_columns = [column_def['from_name'] for column_def in schema_def]
    df = df.select(*select_columns)
    # rename
    data_schema = [T.StructField(column_def['to_name'], column_def['from_type'], True)
                   for column_def in schema_def]
    rename_schema = T.StructType(fields=data_schema)
    df = spark.createDataFrame(df.rdd, schema=rename_schema)
    # cast
    df = df.select([col(column_def['to_name']).cast(column_def['to_type']) for column_def in schema_def])

    df = df.dropDuplicates()

    df = df.na.drop(subset=['iata_code', 'airport_name'])

    df = df.withColumn('coordinates_struct', return_lat_long(col('coordinates'))).drop('coordinates')
    df = df.select('*', "coordinates_struct.latitude").alias('latitude')
    df = df.select('*', "coordinates_struct.longitude").alias('longitude').drop('coordinates_struct')
    column_order = ["iata_code", "airport_name", "airport_type", "airport_country",
                    "airport_region", "municipality", "latitude", "longitude"]
    df = df.select(*column_order)
    df.write.option("compression", "none").parquet(target_path)


def transform_demographic(source_path, target_path,
                          schema_def):
    source_path = os.path.join(source_path, "us-cities-demographics.csv")
    target_path = os.path.join(source_path, "demographic")
    mapping = {'White': 'white',
               'American Indian and Alaska Native': 'native',
               'Hispanic or Latino': 'hispanic',
               'Black or African-American': 'black',
               'Asian': 'asian'}
    city_columns = ['city', 'state', 'state_code', 'foreign_born', 'median_age', 'total_population']
    immigration_columns = ['city', 'census_race', 'census_count']

    df = spark.read.options(delimiter=';', header=True).csv(source_path)
    select_columns = [column_def['from_name'] for column_def in schema_def]
    df = df.select(*select_columns)
    # rename
    data_schema = [T.StructField(column_def['to_name'], column_def['from_type'], True)
                   for column_def in schema_def]
    rename_schema = T.StructType(fields=data_schema)
    df = spark.createDataFrame(df.rdd, schema=rename_schema)
    # cast
    df = df.select([col(column_def['to_name']).cast(column_def['to_type']) for column_def in schema_def])
    # since this is a dimension table -> drop duplicates
    df = df.dropDuplicates()
    # drop n/a on main columns
    df = df.na.drop(subset=['city', 'total_population', 'foreign_born', 'census_race', 'census_count'])
    ## re-arange table structure

    df_city = df.select(*city_columns).dropDuplicates()

    df_immigration = df.select(*immigration_columns).dropDuplicates()
    df_immigration = df_immigration.replace(mapping, subset='census_race')
    df_immigration = df_immigration.groupby(col("city")).pivot("census_race").sum('census_count')
    df_pivoted = df_city.join(df_immigration, ['city'], 'left')
    column_order = ["city", "total_population", "foreign_born", "state", "state_code", "median_age",
                    "asian", "black", "hispanic", "native", "white"]
    df_pivoted = df_pivoted.select(*column_order)
    df_pivoted = df_pivoted.na.drop(subset=['city', 'total_population', "foreign_born"])
    df_pivoted.write.option("compression", "none").parquet(target_path)


def apply_transformations(source_path, target_path):
    _logger.info(f"Start transforming immigration data")
    transform_immigration(source_path, target_path, immigration_column_list)
    _logger.info(f"Start transforming country codes data")
    transform_country_codes_csv(source_path, target_path, country_codes_schema_def)
    _logger.info(f"Start transforming airport codes data")
    transform_airport_codes_csv(source_path, target_path, airport_codes_schema_def)
    _logger.info(f"Start transforming airport info data")
    transform_airport_csv(source_path, target_path, airport_schema_def)
    _logger.info(f"Start transforming city demographic data")
    transform_demographic(source_path, target_path, demographic_schema_def)
