from airflow.hooks.S3_hook import S3Hook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from os.path import isfile, isdir, join
from os import listdir
import os

class CopyToS3Operator(BaseOperator):
    """
    Operator that moves data fransformed to parquet into s3 bucket
    """
    ui_color = '#218251'

    @apply_defaults
    def __init__(self,
                 basepath,
                 s3_bucket,
                 aws_connection_id,
                 aws_default_region,
                 *args, **kwargs):

        super(CopyToS3Operator, self).__init__(*args, **kwargs)
        self.basepath = basepath
        self.s3_bucket = s3_bucket
        self.aws_connection_id = aws_connection_id
        self.aws_default_region = aws_default_region

    def get_file_structure_dict(self, path):
        foldernames = self.get_foldernames(path)
        files_dict = dict()
        for foldername in foldernames:
            for filename in self.get_filenames(os.path.join(path, foldername)):
                files_dict[foldername] = filename
        return files_dict

    def get_filenames(self, path):
        return [f for f in listdir(path) if isfile(join(path, f))]

    def get_foldernames(self, path):
        return [f for f in listdir(path) if isdir(join(path, f))]

    def execute(self, context):
        """
        Load the data files onto the chosen storage location.
        """
        s3_hook = S3Hook(aws_conn_id=self.aws_connection_id)

        files_dict = self.get_file_structure_dict(self.basepath)
        for sub_dir, filename in files_dict.items():
            self.log.info(f'Copy file {sub_dir}/{filename} to s3 bucket {self.s3_bucket}')
            s3_hook.load_file(filename = os.path.join(self.basepath, os.path.join(sub_dir, filename)),
                              key=f"{sub_dir}/{filename}",
                              bucket_name=self.s3_bucket)
