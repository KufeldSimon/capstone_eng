from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

class DataQualityOperator(BaseOperator):
    """
    Apply data quality checks on multiple tables
    """
    ui_color = '#89DA59'

    @apply_defaults
    def __init__(self,
                 redshift_conn_id,
                 dq_checks,
                 *args, **kwargs):

        super(DataQualityOperator, self).__init__(*args, **kwargs)
        self.dq_checks = dq_checks
        self.redshift_conn_id = redshift_conn_id
        self.hook = PostgresHook(postgres_conn_id=self.redshift_conn_id)

    def execute(self, context):
        failing_tests = []
        for d in self.dq_checks:
            sql = d['sql']
            unexp_result = d['unexp_result']
            records = self.hook.get_records(sql)[0]
            if unexp_result == records[0]:
                error_count += 1
                failing_tests.append(sql)
        if error_count > 0:
            self.log.info(f"Tests failed: {failing_tests}")
            raise ValueError('Data quality check failed')
        self.log.info(f"DataQualityOperator: All tables passed the quality check successfully")
