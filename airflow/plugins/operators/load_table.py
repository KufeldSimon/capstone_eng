from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults


class LoadTableOperator(BaseOperator):
    """
    Applies SQL statement on redshift table
    """
    ui_color = '#80BD9E'
    @apply_defaults
    def __init__(self,
                 db,
                 table,
                 redshift_conn_id,
                 dml_query,
                 truncate,
                 ingore_header_rows=1,
                 delimiter=',',
                 *args, **kwargs):

        super(LoadTableOperator, self).__init__(*args, **kwargs)
        self.db = db
        self.table = table
        self.redshift_conn_id = redshift_conn_id
        self.dml_query = dml_query
        self.truncate = truncate
        self.ingore_header_rows = ingore_header_rows
        self.delimiter = delimiter
        self.hook = PostgresHook(postgres_conn_id=self.redshift_conn_id)
        
    def get_dml_expression(self):
        return (
               f"{self.dml_query}"
               f"IGNOREHEADER {self.ingore_header_rows}"
               f"DELIMITER '{self.delimiter}'"
               )

    def execute(self, context):
        dml_expression = self.get_dml_expression()
        if self.truncate:
            self.log.info(f"LoadTableOperator: Truncate {self.db}.{self.table}")
            self.hook.run(f"Truncate TABLE  {self.db}.{self.table}")
        self.log.info(f"LoadTableOperator: Start loading {self.db}.{self.table}")
        self.hook.run(dml_expression, autocommit=True)
        self.log.info(f"LoadTableOperator: Finished loading {self.db}.{self.table}")
