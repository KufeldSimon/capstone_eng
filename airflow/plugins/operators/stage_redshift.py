from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.contrib.hooks.aws_hook import AwsHook


class StageToRedshiftOperator(BaseOperator):
    """
    Loads parquet data from s3 bucket into redshift stage stables
    """
    ui_color = '#358140'

    @apply_defaults
    def __init__(self,
                 ddl_query,
                 db,
                 table,
                 s3_bucket,
                 s3_key,
                 aws_credentials_id,
                 redshift_conn_id,
                 copy_options=None,
                 *args, **kwargs):

        super(StageToRedshiftOperator, self).__init__(*args, **kwargs)
        self.ddl_query = ddl_query
        self.db = db
        self.table = table
        self.s3_source = s3_bucket
        self.s3_key = s3_key
        self.aws_credentials_id = aws_credentials_id
        self.redshift_conn_id = redshift_conn_id
        self.copy_options = copy_options
        self.hook = PostgresHook(postgres_conn_id=self.redshift_conn_id)
        aws_instance = AwsHook(aws_conn_id=self.aws_credentials_id)
        credentials = aws_instance.get_credentials()
        self.aws_access_key_id = credentials.access_key
        self.aws_secret_access_key = credentials.secret_key

    def get_copy_query(self):
        return (
               f"COPY {self.db}.{self.table} FROM 's3://{self.s3_bucket_source}/{self.s3_key}'"
               f"credentials"
               f"'aws_access_key_id={self.aws_access_key_id};aws_secret_access_key={self.aws_secret_access_key}'"
               f"region 'eu-central-1'"
               f"FORMAT AS PARQUET;"
               f"{self.copy_options};"
               )
            
    def execute(self, context):
        copy_query = self.get_copy_query()
        self.log.info(f"StageToRedshiftOperator: Create Staging table {self.db}.{self.table} if not exists")
        self.hook.run(self.ddl_query, autocommit=True)
        self.log.info(f"StageToRedshiftOperator: Delete all rows in {self.db}.{self.table}")
        self.hook.run(f"Truncate TABLE {self.db}.{self.table}")
        self.log.info('StageToRedshiftOperator: Start copying S3 to redshift')
        self.hook.run(copy_query, autocommit=True)
        self.log.info('StageToRedshiftOperator: Finished copying S3 to redshift ')




